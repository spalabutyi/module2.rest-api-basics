package com.epam.esm.controller;

import com.epam.esm.controller.handler.ControllerExceptionHandler;
import com.epam.esm.dao.exception.TagFailCreateException;
import com.epam.esm.dao.exception.TagNotFoundException;
import com.epam.esm.dao.exception.TagUpdateException;
import com.epam.esm.service.TagService;
import com.epam.esm.service.dto.TagDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/tags", produces = MediaType.APPLICATION_JSON_VALUE)
@RestControllerAdvice(basePackages = "com.epam.esm.controller.handler")
public class TagController {

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    /**
     * Retrieves a list of all tags.
     *
     * @return ResponseEntity<List < TagDTO>> containing a list of tags
     * @throws TagNotFoundException if no tags are found
     */
    @GetMapping()
    public ResponseEntity<Object> getAll() {
        List<TagDTO> all = tagService.getAll();

        if (all.isEmpty()) {
            throw new TagNotFoundException();
        }
        return ResponseEntity.ok(all);
    }

    /**
     * Retrieves the tag with the given ID.
     *
     * @param id the ID of the tag to retrieve
     * @return ResponseEntity<TagDTO> containing the retrieved tag
     */
    @GetMapping("/{id}")
    public ResponseEntity<TagDTO> get(@PathVariable("id") Long id) {
        return ResponseEntity.ok(tagService.get(id));
    }

    /**
     * Adds a new tag.
     *
     * @param tagDTO the tag to add
     * @param result the binding result
     * @return ResponseEntity<TagDTO> containing the added tag
     * @throws TagFailCreateException if the tag fails to be created
     */
    @PostMapping()
    public ResponseEntity<TagDTO> add(@RequestBody @Valid TagDTO tagDTO,
                                      BindingResult result) {
        if (result.hasErrors())
            throw new TagFailCreateException(
                    ControllerExceptionHandler.getErrorMessages(result));

        return new ResponseEntity<>(tagService.add(tagDTO), HttpStatus.OK);
    }

    /**
     * Update operation not supported.
     *
     * @return TagUpdateException()
     * @throws TagUpdateException if the update operation is attempted
     */
    @PatchMapping()
    public ResponseEntity<Void> update() {
        throw new TagUpdateException();
    }

    /**
     * Deletes the tag with the given ID.
     *
     * @param id the ID of the tag to delete
     * @return ResponseEntity<HttpStatus> indicating the success of the delete operation
     * @throws TagNotFoundException if the tag is not found
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") long id) {
        if (!tagService.isExists(id)) throw new TagNotFoundException();

        tagService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}