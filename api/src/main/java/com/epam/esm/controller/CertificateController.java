package com.epam.esm.controller;

import com.epam.esm.controller.handler.ControllerExceptionHandler;
import com.epam.esm.dao.exception.CertificateFailCreateException;
import com.epam.esm.dao.exception.CertificateFailUpdateException;
import com.epam.esm.dao.exception.CertificateNotFoundException;
import com.epam.esm.dao.exception.NameExistsException;
import com.epam.esm.service.CertificateService;
import com.epam.esm.service.dto.CertificateDTO;
import com.epam.esm.service.dto.CertificateUpdateDTO;
import com.epam.esm.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;


/**
 * A controller class for managing certificates through REST APIs.
 */
@RestController
@RequestMapping(value = "/api/certificates", produces = MediaType.APPLICATION_JSON_VALUE)
public class CertificateController {

    private final CertificateService certificateService;

    /**
     * Constructor for CertificateController class
     *
     * @param certificateService service used to perform certificate related operations
     */
    @Autowired
    public CertificateController(CertificateService certificateService) {
        this.certificateService = certificateService;
    }

    /**
     * Handles the HTTP GET request to get all certificates
     *
     * @param name        filter parameter for certificate name
     * @param description filter parameter for certificate description
     * @param tagName     filter parameter for certificate tag
     * @param sortBy      parameter for sorting certificates by given field
     * @param sortOrder   parameter for sorting order, ascending or descending
     * @return ResponseEntity containing the list of certificates or error message if not found
     * @throws CertificateNotFoundException if no certificate found
     */
    @GetMapping()
    public ResponseEntity<Object> getAll(@RequestParam(name = "name", required = false) String name,
                                         @RequestParam(name = "description", required = false) String description,
                                         @RequestParam(name = "tag", required = false) String tagName,
                                         @RequestParam(name = "sortBy", required = false) String sortBy,
                                         @RequestParam(name = "sortOrder", required = false) String sortOrder) {

        List<CertificateDTO> certificates;
        if (name == null && description == null && tagName == null) {
            certificates = certificateService.getAll();
        } else {
            certificates = certificateService.findWithParam(name, description, tagName);
        }
        if (certificates.isEmpty()) {
            throw new CertificateNotFoundException();
        }
        if (sortBy != null)
            certificates = certificateService.sort(certificates, sortBy, sortOrder);

        return ResponseEntity.ok(certificates);
    }

    /**
     * Handles the HTTP GET request to get certificate by id
     *
     * @param id certificate id
     * @return ResponseEntity containing the certificate object or error message if not found
     */
    @GetMapping("/{id}")
    public ResponseEntity<CertificateDTO> get(@PathVariable("id") Long id) {
        return ResponseEntity.ok(certificateService.get(id));
    }

    /**
     * Handles the HTTP POST request to add a new certificate
     *
     * @param certificateDTO certificate object to add
     * @param result         validation result
     * @return ResponseEntity containing the added certificate object or error message if validation failed or certificate already exists
     * @throws CertificateFailCreateException if certificate creation fails due to validation errors
     * @throws NameExistsException            if certificate with same name already exists
     */
    @PostMapping()
    public ResponseEntity<CertificateDTO> add(@Validated @RequestBody CertificateDTO certificateDTO,
                                              BindingResult result) {

        if (result.hasErrors())
            throw new CertificateFailCreateException(
                    ControllerExceptionHandler.getErrorMessages(result));

        return new ResponseEntity<>(certificateService.add(certificateDTO), HttpStatus.OK);
    }

    /**
     * Handles the HTTP DELETE request to delete a certificate by id
     *
     * @param id certificate id to delete
     * @return ResponseEntity containing HttpStatus.OK if deleted successfully or error message if certificate not found
     * @throws CertificateNotFoundException if certificate not found
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") Long id) {
        if (!certificateService.isExists(id)) throw new CertificateNotFoundException();

        certificateService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    /**
     * Partially updates a {@link CertificateDTO} resource with the given ID.
     *
     * @param id             the ID of the {@link CertificateDTO} resource to update
     * @param certificateDTO the {@link CertificateUpdateDTO} containing the fields to update
     * @param result         the {@link BindingResult} for the request data validation
     * @return an HTTP response entity with the status {@link HttpStatus#OK} if the update was successful
     * @throws CertificateFailUpdateException if there were validation errors in the request data
     * @throws ServiceException               if the request data is invalid
     * @throws CertificateNotFoundException   if no {@link CertificateDTO} resource was found with the given ID
     */
    @PatchMapping("/{id}")
    public ResponseEntity<HttpStatus> update(@PathVariable("id") Long id,
                                             @Validated @RequestBody CertificateUpdateDTO certificateDTO,
                                             BindingResult result) {
        if (result.hasErrors())
            throw new CertificateFailUpdateException(ControllerExceptionHandler.getErrorMessages(result));
        if (certificateDTO == null) throw new ServiceException("Wrong input data");
        String name = certificateDTO.getName();
        String description = certificateDTO.getDescription();
        Integer duration = certificateDTO.getDuration();
        BigDecimal price = certificateDTO.getPrice();

        CertificateDTO dto = certificateService.get(id);
        if (dto != null) {
            if (name != null) dto.setName(name);
            if (description != null) dto.setDescription(description);
            if (duration != null) dto.setDuration(duration);
            if (price != null) dto.setPrice(price);
        } else {
            throw new CertificateNotFoundException("Wrong ceertificate id " + id);
        }

        certificateService.update(dto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    /**
     * Adds a tag with the given tag ID to the certificate with the given certificate ID.
     *
     * @param certificateId - the ID of the certificate to add the tag to.
     * @param tagId         - the ID of the tag to add to the certificate.
     * @return - a ResponseEntity containing the HTTP status code OK.
     */
    @PostMapping("/{certificateId}/tags/{tagId}")
    public ResponseEntity<HttpStatus> addTagToCertificate(@PathVariable Long certificateId,
                                                          @PathVariable Long tagId) {
        certificateService.addTag(certificateId, tagId);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    /**
     * Deletes the tag with the given tag ID from the certificate with the given certificate ID.
     *
     * @param certificateId - the ID of the certificate to delete the tag from.
     * @param tagId         - the ID of the tag to delete from the certificate.
     * @return - a ResponseEntity containing the HTTP status code OK.
     */
    @DeleteMapping("/{certificateId}/tags/{tagId}")
    public ResponseEntity<HttpStatus> deleteTag(@PathVariable Long certificateId,
                                                @PathVariable Long tagId) {
        certificateService.deleteTag(certificateId, tagId);
        return ResponseEntity.ok(HttpStatus.OK);
    }

}