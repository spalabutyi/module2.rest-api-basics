package com.epam.esm.controller.config.configurer;

import javax.sql.DataSource;

public interface DataSourceConfig {
    DataSource setup();
}