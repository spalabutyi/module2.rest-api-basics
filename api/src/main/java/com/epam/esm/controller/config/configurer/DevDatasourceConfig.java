package com.epam.esm.controller.config.configurer;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
@Profile("dev")
public class DevDatasourceConfig implements DataSourceConfig {

    private static final Logger log = LogManager.getLogger(DevDatasourceConfig.class);

    @Override
    public DataSource setup() {
        HikariConfig config = new HikariConfig("/db/hikari.properties");
        log.debug("profile DEV activated");
        return new HikariDataSource(config);
    }
}