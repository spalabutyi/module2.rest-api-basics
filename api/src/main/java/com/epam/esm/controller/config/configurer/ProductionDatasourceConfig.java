package com.epam.esm.controller.config.configurer;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
@Profile("prod")
public class ProductionDatasourceConfig implements DataSourceConfig {

    private static final Logger log = LogManager.getLogger(ProductionDatasourceConfig.class);

    @Override
    public DataSource setup() {
        HikariConfig config = new HikariConfig("/db/hikari-prod.properties");
        log.debug("profile PROD activated");
        return new HikariDataSource(config);
    }
}
