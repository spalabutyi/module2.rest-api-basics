package com.epam.esm.controller.config.configurer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
@Profile("test")
public class TestDataSourceConfig implements DataSourceConfig {

    private static final Logger log = LogManager.getLogger(TestDataSourceConfig.class);
    public static final String DRIVER = "org.h2.Driver";
    public static final String DATA = "jdbc:h2:tcp://localhost/~/Dropbox/Java/GitLab/Laba/Module#2/RESTAPIBasics/api/src/test/resources/data/dbdata";
    public static final String DATA_MEM = "jdbc:h2:mem:testdb";
    public static final String SA = "sa";

    @Override
    public DataSource setup() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(DRIVER);
        dataSource.setUrl(DATA);
        dataSource.setUsername(SA);
        dataSource.setPassword("");
        log.debug("profile TEST activated");
        return dataSource;
    }
}