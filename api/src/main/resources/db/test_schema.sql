DROP TABLE IF EXISTS gift_certificate CASCADE;

CREATE TABLE IF NOT EXISTS gift_certificate
(
    id               SERIAL PRIMARY KEY,
    name             VARCHAR UNIQUE,
    description      VARCHAR,
    price            DECIMAL,
    duration         INTEGER,
    create_date      TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    last_update_date TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

DROP TABLE IF EXISTS tag CASCADE;

CREATE TABLE IF NOT EXISTS tag
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR UNIQUE
);

DROP TABLE IF EXISTS gift_certificate_tag;

CREATE TABLE IF NOT EXISTS gift_certificate_tag
(
    gift_certificate_id INTEGER REFERENCES gift_certificate (id) ON DELETE CASCADE,
    tag_id              INTEGER REFERENCES tag (id) ON DELETE CASCADE,
    PRIMARY KEY (gift_certificate_id, tag_id)
);

-- Populate the gift_certificate table
INSERT INTO gift_certificate (name, description, price, duration)
VALUES ('Gift Certificate 1', 'Description for Gift Certificate 1', 50.0, 30),
       ('Gift Certificate 2', 'Description for Gift Certificate 2', 100.0, 60),
       ('Gift Certificate 3', 'Description for Gift Certificate 3', 150.0, 90),
       ('Gift Certificate 4', 'Description for Gift Certificate 4', 200.0, 120);

-- Populate the tag table
INSERT INTO tag (name)
VALUES ('Tag 1'),
       ('Tag 2'),
       ('Tag 3'),
       ('Tag 4');

-- Populate the gift_certificate_tag table
INSERT INTO gift_certificate_tag (gift_certificate_id, tag_id)
VALUES (1, 1),
       (1, 2),
       (2, 2),
       (2, 3),
       (3, 3),
       (3, 4),
       (4, 1),
       (4, 4);