-- Create the gifts_db database
-- CREATE DATABASE gifts_db;

-- Create the gift_certificate table
DROP TABLE IF EXISTS gift_certificate CASCADE ;

CREATE TABLE IF NOT EXISTS gift_certificate
(
    id               SERIAL PRIMARY KEY,
    name             VARCHAR UNIQUE,
    description      VARCHAR,
    price            DECIMAL,
    duration         INTEGER,
    create_date      TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    last_update_date TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

-- Create the tag table
DROP TABLE IF EXISTS tag CASCADE ;

CREATE TABLE IF NOT EXISTS tag
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR UNIQUE
);

-- Create the many-to-many relationship between gift_certificate and tag

DROP TABLE IF EXISTS gift_certificate_tag;

CREATE TABLE IF NOT EXISTS gift_certificate_tag
(
    gift_certificate_id INTEGER REFERENCES gift_certificate (id) ON DELETE CASCADE,
    tag_id              INTEGER REFERENCES tag (id) ON DELETE CASCADE,
    PRIMARY KEY (gift_certificate_id, tag_id)
);