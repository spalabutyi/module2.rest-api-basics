### Завдання

#### Бізнес-вимоги
1. Розробити веб-сервіс для системи Подарункових Сертифікатів з наступними сутностями (зв'язок багато-до-багатьох):
   ![](media/model.png)\
   - *CreateDate*, *LastUpdateDate* - формат *ISO 8601* (https://en.wikipedia.org/wiki/ISO_8601). Приклад: 2018-08-29T06:12:15.156. Детальніше про формат тут: https://stackoverflow.com/questions/3914404/how-to-get-current-moment-in-iso-8601-format-with-date-hour-and-minute
   - *Duration* - в днях (термін дії)
2. Система повинна надавати REST API для виконання наступних операцій:
   - CRUD операції для Подарункових Сертифікатів. Якщо при створенні/модифікації передані нові теги - вони повинні бути створені в БД. При операції оновлення - оновлюйте тільки поля, які передані в запиті, інші не повинні бути оновлені. Пакетне вставлення поза областю відповідальності.
   - CRD операції для Тегів.
   - Отримання сертифікатів з тегами (усі параметри є необов'язковими і можуть використовуватися в поєднанні):
      - за назвою тегу (ОДИН тег)
      - пошук за частиною назви/опису (можна реалізувати, використовуючи виклик функції БД)
      - сортування за датою або за ім'ям ASC/DESC (додаткове завдання: реалізувати можливість застосування обох типів сортування одночасно).


#### Вимоги до програми

1. Версія JDK: 8 - використовуйте Streams, java.time.* тощо, де це можливо. (версія JDK може бути збільшена за погодженням з ментором/координатором групи/координатором забігу) 
2. Пакети додатків root: com.epam.esm
3. Будь-який широко використовуваний пул з'єднань може бути використаний. 
4. Для доступу до даних слід використовувати JDBC / Spring JDBC шаблон. 
5. Використовуйте транзакції там, де це необхідно. 
6. Дотримання Java Code Convention є обов'язковим (виняток: розмір поля - 120 символів). 
7. Інструмент збірки: Maven/Gradle, остання версія. Багатомодульний проект. 
8. Веб-сервер: Apache Tomcat/Jetty. 
9. Контейнер додатків: Spring IoC. Spring Framework, остання версія. 
10. База даних: PostgreSQL/MySQL, остання версія.
11. Тестування: JUnit 5.+, Mockito. 
12. Сервісний рівень повинен бути покритий юніт-тестами не менше ніж на 80%. 
13. Рівень репозиторію повинен бути протестований інтеграційними тестами з вбудованою в пам'ять базою даних (всі операції з сертифікатами).    

#### Загальні вимоги

1. Код повинен бути чистим і не містити конструкцій "для розробника".  
2. Додаток повинен бути спроектований та написаний з дотриманням принципів OOD та SOLID. 
3. Код повинен містити цінні коментарі, де це доречно. 
4. Публічні API повинні бути задокументовані (Javadoc). 
5. Слід використовувати чітку багаторівневу структуру з визначеними обов'язками кожного рівня додатку.  
6. JSON має використовуватися як формат комунікаційних повідомлень між клієнтом та сервером.  
7. Повинен бути реалізований зручний механізм обробки помилок/винятків: всі помилки повинні бути осмисленими та локалізованими на стороні бекенду. Приклад: обробка помилки 404: 

        - HTTP Status: 404
        - тіло відповіді    
        - {
        - "errorMessage": "Запитуваний ресурс не знайдено (id = 55)",
        - "errorCode": 40401
        - }
         
    де *errorCode" - ваш власний код (він може базуватися на статусі http і запитуваному ресурсі - сертифікаті або тезі) 
8. Скрізь слід використовувати абстракції, щоб уникнути дублювання коду. 
9. Повинно бути реалізовано кілька конфігурацій (як мінімум дві - dev і prod).

#### Обмеження щодо використання

Заборонено використовувати:
1. Spring Boot.
2. Spring Data Repositories.
3. JPA.
4. Powermock (ваш застосунок має бути тестовним).