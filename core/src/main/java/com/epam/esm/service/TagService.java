package com.epam.esm.service;

import com.epam.esm.service.dto.TagDTO;

public interface TagService extends Service<TagDTO> {

}
