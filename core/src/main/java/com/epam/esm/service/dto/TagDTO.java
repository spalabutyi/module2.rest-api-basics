package com.epam.esm.service.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;

@Setter
@Getter
@Component
public class TagDTO extends AbstractDTO {

    @NotBlank(message = "Name is required")
    @Size(min = 1, max = 16, message = "Name must be between 1 and 16 characters")
    private String name;


    public TagDTO() {
    }

    public TagDTO(String name) {
        this.name = name;
    }

    public TagDTO(Long id, String name) {
        super(id);
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagDTO tagDTO = (TagDTO) o;
        return Objects.equals(name, tagDTO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "TagDTO{id=" + id + '\'' +
                "name='" + name + '\'' +
                '}';
    }
}
