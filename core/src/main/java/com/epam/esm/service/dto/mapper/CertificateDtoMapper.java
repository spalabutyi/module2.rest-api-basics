package com.epam.esm.service.dto.mapper;

import com.epam.esm.model.Certificate;
import com.epam.esm.service.dto.CertificateDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CertificateDtoMapper extends AbstractDtoMapper
        implements DtoMapper<Certificate, CertificateDTO> {

    @Autowired
    public CertificateDtoMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Certificate toEntity(CertificateDTO certificateDTO) {
        return Optional.ofNullable(certificateDTO)
                .map(dto -> modelMapper.map(dto, Certificate.class))
                .orElse(null);
    }

    @Override
    public CertificateDTO toDto(Certificate certificate) {
        return Optional.ofNullable(certificate)
                .map(entity -> modelMapper.map(entity, CertificateDTO.class))
                .orElse(null);
    }
}
