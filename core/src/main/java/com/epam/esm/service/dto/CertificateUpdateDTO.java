package com.epam.esm.service.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Objects;

@Setter
@Getter
@Component
public class CertificateUpdateDTO extends AbstractDTO {

    @Size(min = 1, max = 32, message = "Name must be between 1 and 32 characters")
    private String name;

    @Size(min = 1, max = 256, message = "Description must be between 1 and 256 characters")
    private String description;

    @Positive
    @DecimalMin(value = "0.01", message = "Price must be greater than or equal to 0.01")
    @Digits(integer = 9, fraction = 2, message = "Pattern: xxxxxxxxx.yy")
    private BigDecimal price;

    @Min(value = 1, message = "Duration must be at least 1 day")
    @Max(value = 365, message = "Duration cannot be more than 365 days")
    private Integer duration;

    private CertificateUpdateDTO() {
    }

    public CertificateUpdateDTO(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.description = builder.description;
        this.price = builder.price;
        this.duration = builder.duration;
    }


    public static class Builder {

        private Long id;
        private String name;
        private String description;
        private BigDecimal price;
        private Integer duration;

        public Builder() {
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder price(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder duration(Integer duration) {
            this.duration = duration;
            return this;
        }

        public CertificateUpdateDTO build() {
            return new CertificateUpdateDTO(this);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CertificateUpdateDTO that = (CertificateUpdateDTO) o;
        return Objects.equals(name, that.name) && Objects.equals(description, that.description) && Objects.equals(price, that.price) && Objects.equals(duration, that.duration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, price, duration);
    }

    @Override
    public String toString() {
        return "CertificateUpdateDTO{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", duration=" + duration +
                ", id=" + id +
                '}';
    }
}
