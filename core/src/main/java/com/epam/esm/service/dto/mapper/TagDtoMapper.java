package com.epam.esm.service.dto.mapper;

import com.epam.esm.model.Tag;
import com.epam.esm.service.dto.TagDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TagDtoMapper extends AbstractDtoMapper
        implements DtoMapper<Tag, TagDTO> {

    @Autowired
    public TagDtoMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Tag toEntity(TagDTO tagDTO) {
        return Optional.ofNullable(tagDTO)
                .map(dto -> modelMapper.map(dto, Tag.class))
                .orElse(null);
    }

    @Override
    public TagDTO toDto(Tag tag) {
        return Optional.ofNullable(tag)
                .map(entity -> modelMapper.map(entity, TagDTO.class))
                .orElse(null);
    }
}
