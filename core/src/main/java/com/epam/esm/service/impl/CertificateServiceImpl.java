package com.epam.esm.service.impl;

import com.epam.esm.dao.CertificateDAO;
import com.epam.esm.dao.TagDAO;
import com.epam.esm.dao.exception.CertificateAddTagException;
import com.epam.esm.dao.exception.CertificateNotFoundException;
import com.epam.esm.dao.exception.TagNotFoundException;
import com.epam.esm.model.Certificate;
import com.epam.esm.model.Tag;
import com.epam.esm.service.dto.CertificateDTO;
import com.epam.esm.service.CertificateService;
import com.epam.esm.service.dto.mapper.DtoMapper;
import com.epam.esm.utils.sort.ListSorterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CertificateServiceImpl implements CertificateService {

    public static final String ON_DELETE_MESSAGE = "This certificate does not include this tag";
    public static final String ON_ADD_MESSAGE = "This certificate already includes this tag";
    private final CertificateDAO certificateDAO;
    private final TagDAO tagDAO;
    private final DtoMapper<Certificate, CertificateDTO> certificateDtoMapper;

    @Autowired
    public CertificateServiceImpl(CertificateDAO certificateDAO, TagDAO tagDAO, DtoMapper<Certificate,
            CertificateDTO> certificateDtoMapper) {
        this.certificateDAO = certificateDAO;
        this.tagDAO = tagDAO;
        this.certificateDtoMapper = certificateDtoMapper;
    }

    @Override
    @Transactional
    public CertificateDTO add(CertificateDTO certificateDTO) {
        Certificate certificate = certificateDAO.add(certificateDtoMapper.toEntity(certificateDTO));
        return certificateDtoMapper.toDto(certificate);
    }

    @Override
    @Transactional
    public boolean update(CertificateDTO certificateDTO) {
        return certificateDAO.update(certificateDtoMapper.toEntity(certificateDTO));
    }

    @Override
    public boolean delete(Long id) {
        return certificateDAO.delete(id);
    }

    @Override
    public CertificateDTO get(Long id) {
        CertificateDTO dto = certificateDtoMapper.toDto(certificateDAO.get(id));
        List<Tag> list = tagDAO.getList(id);
        if (!list.isEmpty()) {
            dto.setTags(list);
        }
        return dto;
    }

    @Override
    public List<CertificateDTO> getAll() {
        return certificateDAO.getAll().stream()
                .map(certificateDtoMapper::toDto)
                .peek(dto -> dto.setTags(new ArrayList<>(tagDAO.getList(dto.getId()))))
                .collect(Collectors.toList());
    }

    @Override
    public boolean isExists(Long id) {
        return certificateDAO.isExists(id);
    }

    @Override
    public boolean isExists(String name) {
        return certificateDAO.isExists(name);
    }


    @Override
    public boolean hasTags(Long certificateId, Long tagId) {
        return certificateDAO.hasTags(certificateId, tagId);
    }

    @Override
    public boolean addTag(Long certificateId, Long tagId) {
        if (!isExists(certificateId)) {
            throw new CertificateNotFoundException();
        }
        if (!tagDAO.isExists(tagId)) {
            throw new TagNotFoundException();
        }
        if (hasTags(certificateId, tagId)) {
            throw new CertificateAddTagException(ON_ADD_MESSAGE);
        }

        return certificateDAO.addTag(certificateId, tagId);
    }

    @Override
    public boolean deleteTag(Long certificateId, Long tagId) {
        if (!isExists(certificateId)) {
            throw new CertificateNotFoundException();
        }
        if (!tagDAO.isExists(tagId)) {
            throw new TagNotFoundException();
        }
        if (!hasTags(certificateId, tagId)) {
            throw new CertificateAddTagException(ON_DELETE_MESSAGE);
        }

        return certificateDAO.deleteTag(certificateId, tagId);
    }

    @Override
    public List<CertificateDTO> findWithParam(String name, String description, String tagName) {
        return certificateDAO.findWithParam(name, description, tagName).stream()
                .map(certificateDtoMapper::toDto)
                .peek(dto -> dto.setTags(new ArrayList<>(tagDAO.getList(dto.getId()))))
                .collect(Collectors.toList());
    }

    @Override
    public List<CertificateDTO> sort(List<CertificateDTO> list, String sortBy, String sortOrder) {
        return new ListSorterImpl().sort(list, sortBy, sortOrder);
    }
}