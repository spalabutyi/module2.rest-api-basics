package com.epam.esm.service;

import com.epam.esm.service.dto.CertificateDTO;

import java.util.List;

/**
 * The CertificateService interface extends the Service interface for handling operations related to certificates.
 */
public interface CertificateService extends Service<CertificateDTO> {

    /**
     * Checks if the given certificate has the given tag.
     *
     * @param certificateId The ID of the certificate to check.
     * @param tagId         The ID of the tag to check for.
     * @return true if the certificate has the tag, false otherwise.
     */
    boolean hasTags(Long certificateId, Long tagId);

    /**
     * Adds a tag to the given certificate.
     *
     * @param certificateId The ID of the certificate to add the tag to.
     * @param tagId         The ID of the tag to add.
     * @return true if the tag was added successfully, false otherwise.
     */
    boolean addTag(Long certificateId, Long tagId);

    /**
     * Deletes a tag from the given certificate.
     *
     * @param certificateId The ID of the certificate to delete the tag from.
     * @param tagId         The ID of the tag to delete.
     * @return true if the tag was deleted successfully, false otherwise.
     */
    boolean deleteTag(Long certificateId, Long tagId);

    /**
     * Finds certificates that match the given parameters.
     *
     * @param name        The name of the certificate to find (can be null).
     * @param description The description of the certificate to find (can be null).
     * @param tagName     The name of the tag to find (can be null).
     * @return A list of CertificateDTOs that match the given parameters.
     */
    List<CertificateDTO> findWithParam(String name, String description, String tagName);

    /**
     * Sorts a list of CertificateDTOs by the given field and order.
     *
     * @param list      The list of CertificateDTOs to sort.
     * @param sortBy    The field to sort by (can be null).
     * @param sortOrder The order to sort in (can be null).
     * @return A sorted list of CertificateDTOs.
     */
    List<CertificateDTO> sort(List<CertificateDTO> list, String sortBy, String sortOrder);
}