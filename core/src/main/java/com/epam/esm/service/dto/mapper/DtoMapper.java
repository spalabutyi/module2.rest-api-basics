package com.epam.esm.service.dto.mapper;

/**
 * A generic interface for mapping between data transfer objects (DTOs) and entities.
 *
 * @param <T> The entity type to be mapped to.
 * @param <K> The DTO type to be mapped from.
 */
public interface DtoMapper<T, K> {

    /**
     * Maps the given DTO to an entity.
     *
     * @param k The DTO to be mapped from.
     * @return The resulting entity.
     */
    T toEntity(K k);

    /**
     * Maps the given entity to a DTO.
     *
     * @param t The entity to be mapped from.
     * @return The resulting DTO.
     */
    K toDto(T t);

}