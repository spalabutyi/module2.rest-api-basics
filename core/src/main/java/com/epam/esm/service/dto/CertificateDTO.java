package com.epam.esm.service.dto;

import com.epam.esm.model.Tag;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Setter
@Getter
@Component
public class CertificateDTO extends AbstractDTO {

    @NotBlank(message = "Name is required")
    @Size(min = 1, max = 32, message = "Name must be between 1 and 32 characters")
    private String name;

    @NotBlank(message = "Description is required")
    @Size(min = 1, max = 256, message = "Description must be between 1 and 256 characters")
    private String description;

    @NotNull(message = "Price is required")
    @Positive
    @DecimalMin(value = "0.01", message = "Price must be greater than or equal to 0.01")
    @Digits( integer = 9, fraction = 2, message = "Patter: xxxxxxxxx.yy")
    private BigDecimal price;

    @NotNull(message = "Duration is required")
        @Min(value = 1, message = "Duration must be at least 1 day")
    @Max(value = 365, message = "Duration cannot be more than 365 days")
    private Integer duration;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime createDate;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime lastUpdateDate;

    private List<Tag> tags;

    private CertificateDTO() {
    }

    public CertificateDTO(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.description = builder.description;
        this.price = builder.price;
        this.duration = builder.duration;
        this.createDate = builder.createDate;
        this.lastUpdateDate = builder.lastUpdateDate;
    }


    public static class Builder {

        private Long id;
        private String name;
        private String description;
        private BigDecimal price;
        private Integer duration;
        private LocalDateTime createDate;
        private LocalDateTime lastUpdateDate;
        private List<Tag> tags;

        public Builder() {
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder price(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder duration(Integer duration) {
            this.duration = duration;
            return this;
        }

        public Builder createDate(LocalDateTime createDate) {
            this.createDate = createDate;
            return this;
        }

        public Builder lastUpdateDate(LocalDateTime lastUpdateDate) {
            this.lastUpdateDate = lastUpdateDate;
            return this;
        }

        public Builder listTags(List<Tag> tags) {
            this.tags = tags;
            return this;
        }

        public CertificateDTO build() {
            return new CertificateDTO(this);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CertificateDTO dto = (CertificateDTO) o;
        return Objects.equals(name, dto.name) && Objects.equals(description, dto.description) && Objects.equals(price, dto.price) && Objects.equals(duration, dto.duration) && Objects.equals(createDate, dto.createDate) && Objects.equals(lastUpdateDate, dto.lastUpdateDate) && Objects.equals(tags, dto.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, price, duration, createDate, lastUpdateDate, tags);
    }

    @Override
    public String toString() {
        return "CertificateDTO{" + super.getId() +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", duration=" + duration +
                ", createDate=" + createDate +
                ", lastUpdateDate=" + lastUpdateDate +
                '}';
    }
}
