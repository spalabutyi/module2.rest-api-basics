package com.epam.esm.service.impl;

import com.epam.esm.dao.TagDAO;
import com.epam.esm.model.Tag;
import com.epam.esm.service.dto.TagDTO;
import com.epam.esm.service.TagService;
import com.epam.esm.service.dto.mapper.DtoMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {

    private final TagDAO tagDAO;
    private final DtoMapper<Tag, TagDTO> tagDtoMapper;

    public TagServiceImpl(TagDAO tagDAO, DtoMapper<Tag, TagDTO> tagDtoMapper) {
        this.tagDAO = tagDAO;
        this.tagDtoMapper = tagDtoMapper;
    }


    @Override
    @Transactional
    public TagDTO add(TagDTO tagDTO) {
        Tag tag = tagDAO.add(tagDtoMapper.toEntity(tagDTO));
        return tagDtoMapper.toDto(tag);
    }

    @Override
    public boolean update(TagDTO tagDTO) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Long id) {
        return tagDAO.delete(id);
    }

    @Override
    public TagDTO get(Long id) {
        return tagDtoMapper.toDto(tagDAO.get(id));
    }

    @Override
    public List<TagDTO> getAll() {
        return tagDAO.getAll().stream()
                .map(tagDtoMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isExists(Long id) {
        return tagDAO.isExists(id);
    }

    @Override
    public boolean isExists(String name) {
        return tagDAO.isExists(name);
    }

}