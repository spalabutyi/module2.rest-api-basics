package com.epam.esm.service.dto.mapper;

import org.modelmapper.ModelMapper;

/**
 * Abstract class that provides a base implementation of the DtoMapper interface.
 */
public class AbstractDtoMapper {
    protected ModelMapper modelMapper;
}
