package com.epam.esm.dao.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CertificateNotFoundException extends RuntimeException {

    public CertificateNotFoundException(String message) {
        super(message);
    }
}