package com.epam.esm.dao.exception;

public class CertificateFailCreateException extends RuntimeException{

    public CertificateFailCreateException(String message) {
        super(message);
    }
}
