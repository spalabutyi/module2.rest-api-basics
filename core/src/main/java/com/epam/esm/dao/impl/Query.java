package com.epam.esm.dao.impl;

public abstract class Query {

    public static final String CERTIFICATE_TABLE_NAME = "gift_certificate";
    public static final String TAG_TABLE_NAME = "tag";
    public static final String REF_TABLE_NAME = "gift_certificate_tag";
    public static final String TAG_ID = "tag_id";


    public static final String ADD_TAG = "INSERT INTO tag VALUES (DEFAULT, ?)";
    public static final String GET_TAG = "SELECT * FROM tag WHERE id=?";
    public static final String GET_TAG_BY_NAME = "SELECT * FROM tag WHERE name=?";
    public static final String GET_ALL_TAGS = "SELECT * FROM tag ORDER BY id";
    public static final String DELETE_TAG = "DELETE FROM tag WHERE id=?";
    public static final String CHECK_TAG_EXISTS_BY_ID = "SELECT EXISTS(SELECT * FROM tag WHERE id = ?)";
    public static final String CHECK_TAG_EXISTS_BY_NAME = "SELECT EXISTS(SELECT * FROM tag WHERE name = ?)";
    public static final String GET_CERTIFICATES_TAGS = "SELECT * FROM gift_certificate_tag WHERE gift_certificate_id = ?";


    public static final String ADD_CERTIFICATE = "INSERT INTO gift_certificate VALUES (DEFAULT, ?, ?, ?, ?, ?, ?)";
    public static final String ADD_CERTIFICATES_TAG = "INSERT INTO gift_certificate_tag VALUES (?, ?)";
    public static final String GET_CERTIFICATE = "SELECT * FROM gift_certificate WHERE id=?";
    public static final String GET_AAL_CERTIFICATES = "SELECT * FROM gift_certificate ORDER BY id";
    public static final String DELETE_CERTIFICATE = "DELETE FROM gift_certificate WHERE id=?";
    public static final String DELETE_CERTIFICATES_TAG = "DELETE FROM gift_certificate_tag WHERE gift_certificate_id = ? AND tag_id = ?";
    public static final String UPDATE_CERTIFICATE = "UPDATE gift_certificate SET name=?, description=?, price=?, duration=?, last_update_date=? WHERE id=?";
    public static final String CHECK_CERTIFICATE_EXISTS_BY_ID = "SELECT EXISTS(SELECT * FROM gift_certificate WHERE id = ?)";
    public static final String CHECK_CERTIFICATE_EXISTS_BY_NAME = "SELECT EXISTS(SELECT * FROM gift_certificate WHERE name = ?)";
    public static final String CHECK_CERTIFICATE_HAS_TAGS = "SELECT EXISTS(SELECT * FROM gift_certificate_tag WHERE gift_certificate_id = ? AND tag_id = ?)";
    public static final String FIND_CERTIFICATES_BY_TAG_NAME = "SELECT gc.* FROM gift_certificate gc "
            + "INNER JOIN gift_certificate_tag gct ON gc.id = gct.gift_certificate_id "
            + "INNER JOIN tag t ON t.id = gct.tag_id "
            + "WHERE t.name LIKE ? ORDER BY id";
    public static final String FIND_CERTIFICATES_BY_NAME = "SELECT * FROM gift_certificate WHERE name LIKE ?";
    public static final String FIND_CERTIFICATES_BY_DESCRIPTION = "SELECT * FROM gift_certificate WHERE description LIKE ?";

    private Query() {
    }
}
