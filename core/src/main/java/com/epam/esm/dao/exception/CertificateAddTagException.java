package com.epam.esm.dao.exception;

public class CertificateAddTagException extends RuntimeException{

    public CertificateAddTagException(String message) {
        super(message);
    }

}
