package com.epam.esm.dao.impl;

import com.epam.esm.dao.AbstractDAO;
import com.epam.esm.dao.TagDAO;
import com.epam.esm.dao.exception.NameExistsException;
import com.epam.esm.dao.exception.TagNotFoundException;
import com.epam.esm.model.Tag;
import com.epam.esm.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;

@Component
public class TagDAOImpl extends AbstractDAO implements TagDAO {

    public static final String MESSAGE = "Tag was not added";

    @Autowired
    public TagDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Tag add(Tag tag) {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            int update = jdbcTemplate.update(
                    connection -> {
                        PreparedStatement ps = connection.prepareStatement(Query.ADD_TAG, new String[]{"id"});
                        ps.setString(1, tag.getName());
                        return ps;
                    },
                    keyHolder
            );

            if (update == 0) {
                throw new ServiceException(MESSAGE);
            }

            long generatedId;
            generatedId = Objects.requireNonNull(keyHolder.getKey()).longValue();

            tag.setId(generatedId);

            return tag;
        } catch (DuplicateKeyException ex) {
            throw new NameExistsException(tag.getName());
        }
    }

    @Override
    public boolean update(Tag tag) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Long id) {
        return jdbcTemplate.update(Query.DELETE_TAG, id) > 0;
    }

    @Override
    public Tag get(Long id) {
        return jdbcTemplate.query(Query.GET_TAG, new Object[]{id},
                        new BeanPropertyRowMapper<>(Tag.class)).stream()
                .findAny()
                .orElseThrow(TagNotFoundException::new);
    }

    @Override
    public List<Tag> getAll() {
        return jdbcTemplate.query(Query.GET_ALL_TAGS,
                new BeanPropertyRowMapper<>(Tag.class));
    }

    @Override
    public boolean isExists(Long id) {
        return Boolean.TRUE.equals(jdbcTemplate.queryForObject(
                Query.CHECK_TAG_EXISTS_BY_ID, Boolean.class, id));
    }

    @Override
    public boolean isExists(String name) {
        return Boolean.TRUE.equals(jdbcTemplate.queryForObject(
                Query.CHECK_TAG_EXISTS_BY_NAME, Boolean.class, name));
    }

    @Override
    public List<Tag> getList(Long id) {
        return jdbcTemplate.query(Query.GET_CERTIFICATES_TAGS, new Object[]{id},
                (resultSet, rowNum) ->
                        get(resultSet.getLong(Query.TAG_ID))
        );
    }

}
