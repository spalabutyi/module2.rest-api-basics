package com.epam.esm.dao.impl;

import com.epam.esm.dao.AbstractDAO;
import com.epam.esm.dao.CertificateDAO;
import com.epam.esm.dao.exception.CertificateNotFoundException;
import com.epam.esm.dao.exception.NameExistsException;
import com.epam.esm.model.Certificate;
import com.epam.esm.service.exception.ServiceException;
import com.epam.esm.utils.CertificateSearchQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Component
public class CertificateDAOImpl extends AbstractDAO implements CertificateDAO {

    public static final String MESSAGE = "Certificate was not added";

    @Autowired
    public CertificateDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Certificate add(Certificate certificate) {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            LocalDateTime dateTime = LocalDateTime.now();
            int update = jdbcTemplate.update(
                    connection -> {
                        PreparedStatement ps = connection.prepareStatement(Query.ADD_CERTIFICATE, new String[]{"id"});
                        ps.setString(1, certificate.getName());
                        ps.setString(2, certificate.getDescription());
                        ps.setBigDecimal(3, certificate.getPrice());
                        ps.setInt(4, certificate.getDuration());
                        for (int i = 5; i < 7; i++) ps.setTimestamp(i, Timestamp.valueOf(dateTime));
                        return ps;
                    },
                    keyHolder
            );

            if (update == 0) {
                throw new ServiceException(MESSAGE);
            }

            long generatedId;
            generatedId = Objects.requireNonNull(keyHolder.getKey()).longValue();

            certificate.setId(generatedId);
            certificate.setCreateDate(dateTime);
            certificate.setLastUpdateDate(dateTime);

            return certificate;
        } catch (DuplicateKeyException ex) {
            throw new NameExistsException(certificate.getName());
        }
    }

    @Override
    public boolean update(Certificate certificate) {
        LocalDateTime dateTime = LocalDateTime.now();
        return jdbcTemplate.update(Query.UPDATE_CERTIFICATE,
                certificate.getName(), certificate.getDescription(),
                certificate.getPrice(), certificate.getDuration(),
                dateTime, certificate.getId()) > 0;
    }

    @Override
    public boolean delete(Long id) {
        return jdbcTemplate.update(Query.DELETE_CERTIFICATE, id) > 0;
    }

    @Override
    public Certificate get(Long id) {
        return jdbcTemplate.query(Query.GET_CERTIFICATE, new Object[]{id},
                        new BeanPropertyRowMapper<>(Certificate.class)).stream()
                .findAny().orElseThrow(CertificateNotFoundException::new);
    }

    @Override
    public List<Certificate> getAll() {
        return jdbcTemplate.query(Query.GET_AAL_CERTIFICATES,
                new BeanPropertyRowMapper<>(Certificate.class));
    }

    @Override
    public boolean isExists(Long id) {
        return Boolean.TRUE.equals(jdbcTemplate.queryForObject(
                Query.CHECK_CERTIFICATE_EXISTS_BY_ID, Boolean.class, id));
    }

    @Override
    public boolean isExists(String name) {
        return Boolean.TRUE.equals(jdbcTemplate.queryForObject(
                Query.CHECK_CERTIFICATE_EXISTS_BY_NAME, Boolean.class, name));
    }

    @Override
    public boolean hasTags(Long certificateId, Long tagId) {
        return Boolean.TRUE.equals(jdbcTemplate.queryForObject(
                Query.CHECK_CERTIFICATE_HAS_TAGS, Boolean.class, certificateId, tagId));
    }

    @Override
    public boolean addTag(Long certificateId, Long tagId) {
        return jdbcTemplate.update(Query.ADD_CERTIFICATES_TAG,
                certificateId, tagId) > 0;
    }

    @Override
    public boolean deleteTag(Long certificateId, Long tagId) {
        return jdbcTemplate.update(Query.DELETE_CERTIFICATES_TAG,
                certificateId, tagId) > 0;
    }

    @Override
    public List<Certificate> findWithParam(String name, String description, String tagName) {
        String searchQuery = CertificateSearchQueryFactory.getSearchQuery(name, description, tagName);

        return jdbcTemplate.query(searchQuery, new BeanPropertyRowMapper<>(Certificate.class));
    }
}
