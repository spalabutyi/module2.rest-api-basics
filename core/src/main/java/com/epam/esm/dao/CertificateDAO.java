package com.epam.esm.dao;

import com.epam.esm.model.Certificate;

import java.util.List;

public interface CertificateDAO extends Dao<Certificate> {

    /**
     * Returns true if a given certificate has a given tag associated with it.
     *
     * @param certificateId The ID of the certificate to check for the tag.
     * @param tagId         The ID of the tag to check if it is associated with the certificate.
     * @return A boolean value indicating whether the certificate has the tag.
     */
    boolean hasTags(Long certificateId, Long tagId);

    /**
     * Associates a given tag with a given certificate.
     *
     * @param certificateId The ID of the certificate to add the tag to.
     * @param tagId         The ID of the tag to add to the certificate.
     * @return A boolean value indicating whether the tag was successfully added to the certificate.
     */
    boolean addTag(Long certificateId, Long tagId);

    /**
     * Removes a given tag from a given certificate.
     *
     * @param certificateId The ID of the certificate to remove the tag from.
     * @param tagId         The ID of the tag to remove from the certificate.
     * @return A boolean value indicating whether the tag was successfully removed from the certificate.
     */
    boolean deleteTag(Long certificateId, Long tagId);

    /**
     * Finds a list of certificates that match the given search criteria.
     *
     * @param name        The name to search for in certificate entities.
     * @param description The description to search for in certificate entities.
     * @param tagName     The tag name to search for in certificate entities.
     * @return A list of Certificate objects that match the search criteria.
     */
    List<Certificate> findWithParam(String name, String description, String tagName);
}