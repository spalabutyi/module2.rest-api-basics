package com.epam.esm.dao.mapper;

import com.epam.esm.model.Certificate;
import org.springframework.jdbc.core.RowMapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CertificateMapper implements RowMapper<Certificate> {

    @Override
    public Certificate mapRow(ResultSet rs, int rowNum) throws SQLException {

        return new Certificate.Builder()
                .id(rs.getLong(Constants.ID))
                .name(rs.getString(Constants.NAME))
                .duration(rs.getInt(Constants.DURATION))
                .description(rs.getString(Constants.DESCRIPTION))
                .price(BigDecimal.valueOf(rs.getDouble(Constants.PRICE)))
                .createDate(rs.getTimestamp(Constants.CREATE_DATE).toLocalDateTime())
                .lastUpdateDate(rs.getTimestamp(Constants.LAST_UPDATE_DATE).toLocalDateTime())
                .build();
    }
}
