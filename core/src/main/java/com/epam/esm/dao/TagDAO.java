package com.epam.esm.dao;

import com.epam.esm.model.Tag;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface TagDAO extends Dao<Tag> {

   List<Tag> getList(Long id);

}
