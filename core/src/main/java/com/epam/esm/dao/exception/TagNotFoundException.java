package com.epam.esm.dao.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TagNotFoundException extends RuntimeException {

    public TagNotFoundException(String message) {
        super(message);
    }

}
