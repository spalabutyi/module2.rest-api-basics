package com.epam.esm.dao.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TagFailCreateException extends RuntimeException{

    public TagFailCreateException(String message) {
        super(message);
    }
}
