package com.epam.esm.dao.exception;

public class CertificateFailUpdateException extends RuntimeException{

    public CertificateFailUpdateException(String message) {
        super(message);
    }

}
