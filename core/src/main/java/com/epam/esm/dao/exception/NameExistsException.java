package com.epam.esm.dao.exception;

public class NameExistsException extends RuntimeException{

    public NameExistsException(String message) {
        super(message);
    }
}
