package com.epam.esm.utils;

import com.epam.esm.service.exception.ServiceException;

/**
 * The CertificateSearchQueryFactory class is responsible for creating search queries
 * based on search criteria for gift certificates.
 */
public class CertificateSearchQueryFactory {

    public static final String MESSAGE = "Invalid search criteria";
    public static final String SELECT_AND_INNER_JOIN = "SELECT gc.* FROM gift_certificate gc " +
            "INNER JOIN gift_certificate_tag gct ON gc.id = gct.gift_certificate_id ";
    public static final String SELECT_FROM = "SELECT * FROM gift_certificate WHERE";
    public static final String ADD_INNER_JOIN = "INNER JOIN tag t ON t.id = gct.tag_id WHERE";
    public static final String ADD_AND_TNAME = "%' AND t.name = '";
    public static final String ADD_LIKE = "LIKE '%";

    /**
     * Returns the search query based on the search criteria for gift certificates.
     *
     * @param certificateName        The name of the gift certificate to search for.
     * @param certificateDescription The description of the gift certificate to search for.
     * @param tagName                The name of the tag to search for.
     * @return The search query as a String.
     * @throws ServiceException If an invalid search criteria is passed to the method.
     */
    public static String getSearchQuery(String certificateName, String certificateDescription, String tagName) {
        if (certificateName != null && certificateDescription == null && tagName == null) {
            // Case 1. Search by certificate.name only
            return SELECT_FROM + " name " + ADD_LIKE + certificateName + "%'";

        } else if (certificateName == null && certificateDescription != null && tagName == null) {
            // Case 2. Search by certificate.description only
            return SELECT_FROM + " description " + ADD_LIKE + certificateDescription + "%'";

        } else if (certificateName == null && certificateDescription == null && tagName != null) {
            // Case 3. Search by tag.name only
            return SELECT_AND_INNER_JOIN + "INNER JOIN tag t ON t.id = gct.tag_id WHERE t.name = '" + tagName + "'";

        } else if (certificateName != null && certificateDescription == null) {
            // Case 4. Search by certificate.name and tag.name
            return SELECT_AND_INNER_JOIN + ADD_INNER_JOIN +
                    " gc.name " + ADD_LIKE + certificateName + ADD_AND_TNAME + tagName + "'";

        } else if (certificateName == null
                && certificateDescription != null) {
            // Case 5. Search by  certificate.description and tag.name
            return SELECT_AND_INNER_JOIN + ADD_INNER_JOIN +
                    " gc.description " + ADD_LIKE + certificateDescription + ADD_AND_TNAME + tagName + "'";

            // Cases 6 & 7. All and any.
        } else if (certificateName != null && tagName != null) {
            throw new ServiceException(MESSAGE);
        } else throw new ServiceException(MESSAGE);
    }
}