package com.epam.esm.utils.sort;

import com.epam.esm.service.dto.CertificateDTO;
import com.epam.esm.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class ListSorterImpl implements ListSorter<CertificateDTO> {

    public static final String BY_DATE = "date";
    public static final String BY_NAME = "name";
    public static final String REVERSE_ORDER = "DESC";

    /**
     * This method sorts a list of CertificateDTO objects based on a specified field and order.
     *
     * @param list      The list of CertificateDTO objects to be sorted.
     * @param sortBy    The field to sort by. It can be "BY_NAME" or "BY_DATE".
     * @param sortOrder The order of sorting. It can be "REVERSE_ORDER" or null for ascending order.
     * @return A sorted list of CertificateDTO objects.
     * @throws ServiceException if the sortBy parameter is not "BY_NAME" or "BY_DATE".
     */
    @Override
    public List<CertificateDTO> sort(List<CertificateDTO> list, String sortBy, String sortOrder) {
        List<CertificateDTO> sorted = new ArrayList<>(list);

        Comparator<CertificateDTO> comparator;
        switch (sortBy) {
            case BY_NAME:
                comparator = Comparator.comparing(CertificateDTO::getName);
                break;
            case BY_DATE:
                comparator = Comparator.comparing(CertificateDTO::getCreateDate);
                break;
            default:
                throw new ServiceException("Unknown sorting field: " + sortBy);
        }

        if (Objects.equals(sortOrder, REVERSE_ORDER)) {
            comparator = comparator.reversed();
        }

        sorted.sort(comparator);
        return sorted;
    }
}
