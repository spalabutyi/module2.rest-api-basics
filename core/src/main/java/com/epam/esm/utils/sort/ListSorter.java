package com.epam.esm.utils.sort;

import java.util.List;

/**
 * Interface for sorting a list of objects of type T.
 *
 * @param <T> the type of object to be sorted
 */
public interface ListSorter<T> {

    /**
     * Sorts a given list of objects of type T based on a specified property and sorting order.
     *
     * @param list      the list of objects to be sorted
     * @param sortBy    the property to sort the objects by
     * @param sortOrder the sorting order to use ("asc" or "desc")
     * @return a sorted list of objects of type T
     */
    List<T> sort(List<T> list, String sortBy, String sortOrder);
}