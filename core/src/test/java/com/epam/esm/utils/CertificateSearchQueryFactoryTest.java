package com.epam.esm.utils;

import com.epam.esm.service.exception.ServiceException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("CertificateSearchQueryFactory")
class CertificateSearchQueryFactoryTest {

    public static final String GIFT_CERTIFICATE = "Gift Certificate";
    public static final String BEST_GIFT = "Best Gift";
    public static final String CHRISTMAS = "Christmas";

    @Test
    @DisplayName("Should return correct search query for certificate name")
    void shouldReturnSearchQueryForCertificateName() {
        String searchQuery = CertificateSearchQueryFactory.getSearchQuery(GIFT_CERTIFICATE, null, null);
        assertEquals("SELECT * FROM gift_certificate WHERE name LIKE '%Gift Certificate%'", searchQuery);
    }

    @Test
    @DisplayName("Should return correct search query for certificate description")
    void shouldReturnSearchQueryForCertificateDescription() {
        String searchQuery = CertificateSearchQueryFactory.getSearchQuery(null, BEST_GIFT, null);
        assertEquals("SELECT * FROM gift_certificate WHERE description LIKE '%Best Gift%'", searchQuery);
    }

    @Test
    @DisplayName("Should return correct search query for tag name")
    void shouldReturnSearchQueryForTagName() {
        String searchQuery = CertificateSearchQueryFactory.getSearchQuery(null, null, CHRISTMAS);
        assertEquals("SELECT gc.* FROM gift_certificate gc INNER JOIN gift_certificate_tag gct ON gc.id = gct.gift_certificate_id INNER JOIN tag t ON t.id = gct.tag_id WHERE t.name = 'Christmas'", searchQuery);
    }

    @Test
    @DisplayName("Should return correct search query for certificate name and tag name")
    void shouldReturnSearchQueryForCertificateNameAndTagName() {
        assertThrows(ServiceException.class, () -> CertificateSearchQueryFactory.getSearchQuery(GIFT_CERTIFICATE, "null", CHRISTMAS));
    }

    @Test
    @DisplayName("Should throw exception for invalid search criteria")
    void shouldThrowExceptionForInvalidSearchCriteria() {
        assertThrows(ServiceException.class, () -> CertificateSearchQueryFactory.getSearchQuery(GIFT_CERTIFICATE, BEST_GIFT, CHRISTMAS));
    }

}