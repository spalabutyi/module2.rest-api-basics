package com.epam.esm.utils.sort;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.epam.esm.service.exception.ServiceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.epam.esm.service.dto.CertificateDTO;

@DisplayName("Tests for ListSorterImpl class")
class ListSorterImplTest {

    private ListSorter<CertificateDTO> sorter;
    private List<CertificateDTO> certificates;

    @BeforeEach
    void setUp() {
        sorter = new ListSorterImpl();

        certificates = new ArrayList<>();
        certificates.add(new CertificateDTO.Builder().id(1L).name("Certificate 1").description("Description 1").duration(10).createDate(LocalDateTime.of(2022, 4, 1, 12, 0)).build());
        certificates.add(new CertificateDTO.Builder().id(2L).name("Certificate 2").description("Description 2").duration(20).createDate(LocalDateTime.of(2022, 4, 2, 12, 0)).build());
        certificates.add(new CertificateDTO.Builder().id(3L).name("Certificate 3").description("Description 3").duration(30).createDate(LocalDateTime.of(2022, 4, 3, 12, 0)).build());
    }

    @Test
    @DisplayName("Sort by name in ascending order")
    void sortByNameAscending() {
        List<CertificateDTO> sorted = sorter.sort(certificates, ListSorterImpl.BY_NAME, null);

        assertEquals("Certificate 1", sorted.get(0).getName());
        assertEquals("Certificate 2", sorted.get(1).getName());
        assertEquals("Certificate 3", sorted.get(2).getName());
    }

    @Test
    @DisplayName("Sort by name in descending order")
    void sortByNameDescending() {
        List<CertificateDTO> sorted = sorter.sort(certificates, ListSorterImpl.BY_NAME, ListSorterImpl.REVERSE_ORDER);

        assertEquals("Certificate 3", sorted.get(0).getName());
        assertEquals("Certificate 2", sorted.get(1).getName());
        assertEquals("Certificate 1", sorted.get(2).getName());
    }

    @Test
    @DisplayName("Sort by date in ascending order")
    void sortByDateAscending() {
        List<CertificateDTO> sorted = sorter.sort(certificates, ListSorterImpl.BY_DATE, null);

        assertEquals(LocalDateTime.of(2022, 4, 1, 12, 0), sorted.get(0).getCreateDate());
        assertEquals(LocalDateTime.of(2022, 4, 2, 12, 0), sorted.get(1).getCreateDate());
        assertEquals(LocalDateTime.of(2022, 4, 3, 12, 0), sorted.get(2).getCreateDate());
    }

    @Test
    @DisplayName("Sort by date in descending order")
    void sortByDateDescending() {
        List<CertificateDTO> sorted = sorter.sort(certificates, ListSorterImpl.BY_DATE, ListSorterImpl.REVERSE_ORDER);

        assertEquals(LocalDateTime.of(2022, 4, 3, 12, 0), sorted.get(0).getCreateDate());
        assertEquals(LocalDateTime.of(2022, 4, 2, 12, 0), sorted.get(1).getCreateDate());
        assertEquals(LocalDateTime.of(2022, 4, 1, 12, 0), sorted.get(2).getCreateDate());
    }

    @Test
    @DisplayName("Throw exception when sortBy is invalid")
    void testSortWithInvalidSortBy() {
        assertThrows(ServiceException.class, () -> sorter.sort(certificates, "invalid", null));
    }
}

