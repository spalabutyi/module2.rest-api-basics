package com.epam.esm.dao.impl;

import com.epam.esm.config.TestConfig;
import com.epam.esm.dao.CertificateDAO;
import com.epam.esm.dao.exception.NameExistsException;
import com.epam.esm.model.Certificate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@Transactional
@ActiveProfiles("test")
@ContextConfiguration(classes = {TestConfig.class})
class CertificateDAOImplTest {

    private static final Logger log = LogManager.getLogger(CertificateDAOImplTest.class);

    @Autowired
    private CertificateDAO certificateDAO;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    @Transactional
    void getAllCertificates_test() {
        List<Certificate> list = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            Certificate certificateDTO = new Certificate.Builder()
                    .id((long) i)
                    .name("Gift Certificate " + i)
                    .description("Description for Gift Certificate " + i)
                    .price(BigDecimal.valueOf(50.0 * i))
                    .duration(30 * i)
                    .build();
            list.add(certificateDTO);
        }
        List<Certificate> repositoryList = certificateDAO.getAll();
        for (int i = 0; i <= 3; i++) {
            assertEquals(list.get(i).getId(), repositoryList.get(i).getId());
            assertEquals(list.get(i).getName(), repositoryList.get(i).getName());
            assertEquals(list.get(i).getDescription(), repositoryList.get(i).getDescription());
            assertEquals(list.get(i).getPrice(), repositoryList.get(i).getPrice());
            assertEquals(list.get(i).getDuration(), repositoryList.get(i).getDuration());
        }
    }

    @Test
    void saveAndDeleteCertificateTest() {
        Certificate certificateDTO = new Certificate.Builder()
                .name("Gift Certificate 5")
                .description("Description for Gift Certificate 5")
                .price(BigDecimal.valueOf(55))
                .duration(33)
                .build();

        Certificate added = certificateDAO.add(certificateDTO);
        List<Certificate> repositoryList = certificateDAO.getAll();
        assertAll(
                () -> assertEquals(added.getName(), repositoryList.get(repositoryList.size() - 1).getName()),
                () -> assertThrows(NameExistsException.class, () -> certificateDAO.add(certificateDTO)))
        ;
        boolean deleted = certificateDAO.delete(added.getId());
        assertEquals(Boolean.TRUE, deleted);

        boolean reDeleted = certificateDAO.delete(added.getId());
        assertEquals(Boolean.FALSE, reDeleted);

    }

    @Test
    void updateCertificateTest() {

        Certificate certificateDTO = new Certificate.Builder()
                .name("Gift Certificate 5")
                .description("Description for Gift Certificate 5")
                .price(BigDecimal.valueOf(55))
                .duration(33)
                .build();

        Certificate added = certificateDAO.add(certificateDTO);

        Certificate updatesCertificateDTO = new Certificate.Builder()
                .id(added.getId())
                .name("Gift Certificate Updated")
                .description("Updated Description for Gift Certificate 5")
                .price(BigDecimal.valueOf(5))
                .duration(1)
                .build();

        boolean updated = certificateDAO.update(updatesCertificateDTO);
        assertEquals(Boolean.TRUE, updated);

        Certificate certificate = certificateDAO.get(updatesCertificateDTO.getId());
        assertAll(
                () -> assertEquals(certificate.getName(), updatesCertificateDTO.getName()),
                () -> assertEquals(certificate.getDescription(), updatesCertificateDTO.getDescription()),
                () -> assertEquals(certificate.getDuration(), updatesCertificateDTO.getDuration()),
                () -> assertEquals(certificate.getPrice(), updatesCertificateDTO.getPrice())
        );

        certificateDAO.delete(added.getId());
    }

    @Test
    void certificateExistenceTest() {
        boolean existsCase1 = certificateDAO.isExists(1L);
        boolean existsCase2 = certificateDAO.isExists(10L);
        boolean existsCase3 = certificateDAO.isExists("Gift Certificate 1");
        boolean existsCase4 = certificateDAO.isExists("Gift Certificate 10");

        assertAll(
                () -> assertEquals(Boolean.TRUE, existsCase1),
                () -> assertEquals(Boolean.FALSE, existsCase2),
                () -> assertEquals(Boolean.TRUE, existsCase3),
                () -> assertEquals(Boolean.FALSE, existsCase4)
        );
    }

    @Test
    void certificateTagOperationsTest() {

        boolean existsCase1 = certificateDAO.hasTags(1L, 1L);
        boolean existsCase2 = certificateDAO.hasTags(10L, 10L);
        boolean addedTad = certificateDAO.addTag(1L, 4L);
        boolean deletedTag = certificateDAO.deleteTag(1L, 1L);

        assertAll(
                () -> assertEquals(Boolean.TRUE, existsCase1),
                () -> assertEquals(Boolean.FALSE, existsCase2),
                () -> assertEquals(Boolean.TRUE, addedTad),
                () -> assertEquals(Boolean.TRUE, deletedTag),
                () -> assertEquals(Boolean.FALSE, certificateDAO.deleteTag(1L, 1L)),
                () -> assertThrows(DuplicateKeyException.class,
                        () -> certificateDAO.addTag(1L, 4L))
        );
    }

    @Test
    void findWithParametersTest() {
        String validName = "Gift Certificate 1";
        String validDescription = "Description for Gift Certificate 1";
        String validTagName = "Tag 1";

        String invalidName = "Any_Certificate";
        String invalidTagName = "Any_TagName";

        List<Certificate> listWithValidParams1 = certificateDAO.findWithParam(validName, null, validTagName);
        List<Certificate> listWithValidParams2 = certificateDAO.findWithParam(null, validDescription, validTagName);
        List<Certificate> listWithInvalidParams1 = certificateDAO.findWithParam(invalidName, null, validTagName);
        List<Certificate> listWithInvalidParams2 = certificateDAO.findWithParam(null, validDescription, invalidTagName);

        assertAll(
                () -> assertTrue(listWithValidParams1.size() > 0),
                () -> assertTrue(listWithValidParams2.size() > 0),
                () -> assertFalse(listWithInvalidParams1.size() > 0),
                () -> assertFalse(listWithInvalidParams2.size() > 0)
        );
    }
}