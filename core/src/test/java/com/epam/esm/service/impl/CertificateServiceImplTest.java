package com.epam.esm.service.impl;

import com.epam.esm.dao.CertificateDAO;
import com.epam.esm.dao.TagDAO;
import com.epam.esm.dao.exception.CertificateAddTagException;
import com.epam.esm.dao.exception.CertificateNotFoundException;
import com.epam.esm.dao.exception.TagNotFoundException;
import com.epam.esm.model.Certificate;
import com.epam.esm.model.Tag;
import com.epam.esm.service.CertificateService;
import com.epam.esm.service.dto.CertificateDTO;
import com.epam.esm.service.dto.mapper.CertificateDtoMapper;
import com.epam.esm.service.dto.mapper.DtoMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CertificateServiceImplTest {

    @Mock
    private CertificateDAO certificateDAO;
    @Mock
    private TagDAO tagDAO;
    private DtoMapper<Certificate, CertificateDTO> certificateDTOMapper;
    private CertificateService certificateService;
    private final CertificateDTO certificate = new CertificateDTO.Builder()
            .id(1L)
            .name("Certificate")
            .description("Test certificate")
            .duration(100)
            .createDate(LocalDateTime.now())
            .lastUpdateDate(LocalDateTime.now())
            .build();
    private static final Tag tag = new Tag(1L, "Test Tag");

    @BeforeEach
    void setUp() {
        certificateDTOMapper = new CertificateDtoMapper(new ModelMapper());
        certificateService = new CertificateServiceImpl(certificateDAO, tagDAO, certificateDTOMapper);
    }

    @Test
    void getAllTest() {

        List<Certificate> list = new ArrayList<>();
        list.add((certificateDTOMapper.toEntity(certificate)));

        when(certificateDAO.getAll()).thenReturn(list);

        List<CertificateDTO> all = certificateService.getAll();
        verify(certificateDAO).getAll();
        assertTrue(all.size() > 0);
    }

    @Test
    void addCertificateTest() {

        Certificate entity = certificateDTOMapper.toEntity(certificate);
        when(certificateDAO.add(any(Certificate.class)))
                .thenReturn(entity);
        CertificateDTO added = certificateService.add(certificate);
        assertNotNull(added.getName());

    }

    @Test
    void getTest() {
        certificateService.get(certificate.getId());

        ArgumentCaptor<Long> argumentCaptor =
                ArgumentCaptor.forClass(Long.class);

        verify(certificateDAO).get(argumentCaptor.capture());

        Long value = argumentCaptor.getValue();

        assertEquals(value, certificateDTOMapper.toEntity(certificate).getId());
    }

    @Test
    void deleteTest() {
        certificateService.delete(certificate.getId());
        verify(certificateDAO).delete(certificate.getId());

        when(certificateDAO.delete(eq(certificate.getId()))).thenReturn(true);

        assertTrue(certificateService.delete(certificate.getId()));
        assertFalse(certificateService.delete(2L));
    }

    @Test
    void isExistsTest() {

        given(certificateDAO.isExists(anyString())).willReturn(true);
        given(certificateDAO.isExists(anyLong())).willReturn(false);

        boolean nameExists = certificateService.isExists(certificate.getName());
        boolean idExists = certificateService.isExists(certificate.getId());

        verify(certificateDAO).isExists(anyString());
        assertTrue(nameExists);
        verify(certificateDAO).isExists(anyLong());
        assertFalse(idExists);
    }

    @Test
    void updateCertificateTest() {

        when(certificateDAO.update(any(Certificate.class)))
                .thenReturn(true);

        boolean updated = certificateService.update(certificate);
        assertTrue(updated);
        verify(certificateDAO).update(any(Certificate.class));
    }

    @Test
    void isExistTest(){
        String name = certificate.getName();
        Long id = certificate.getId();


        when(certificateDAO.isExists(anyString())).thenReturn(true);
        when(certificateDAO.isExists(anyLong())).thenReturn(false);

        assertTrue(certificateService.isExists(name));
        verify(certificateDAO).isExists(name);
        assertFalse(certificateService.isExists(id));
        verify(certificateDAO).isExists(id);
    }

    @Test
    void findTagTests () {

        when(certificateDAO.hasTags(anyLong(), anyLong())).thenReturn(true);
        boolean hasTags = certificateService.hasTags(certificate.getId(), tag.getId());
        assertTrue(hasTags);
        verify(certificateDAO).hasTags(1L, 1L);
    }


    @Test
    void addTagTests_Success() {

        when(certificateDAO.addTag(anyLong(), anyLong())).thenReturn(true);
        when(certificateDAO.isExists(anyLong())).thenReturn(true);
        when(tagDAO.isExists(anyLong())).thenReturn(true);

        boolean addTag = certificateService.addTag(certificate.getId(), tag.getId());
        assertTrue(addTag);
        verify(certificateDAO).addTag(1L, 1L);

    }
    @Test
    void addTagTests_Fail () {

        when(certificateDAO.isExists(anyLong())).thenReturn(false);
        assertThrows(CertificateNotFoundException.class,
                () -> certificateService.addTag(certificate.getId(), tag.getId()));

        when(certificateDAO.isExists(anyLong())).thenReturn(true);
        when(tagDAO.isExists(anyLong())).thenReturn(false);

        assertThrows(TagNotFoundException.class,
                () -> certificateService.addTag(certificate.getId(), tag.getId()));

    }

    @Test
    void deleteTagTests_Success() {

        when(certificateDAO.deleteTag(anyLong(), anyLong())).thenReturn(true);
        when(certificateDAO.isExists(anyLong())).thenReturn(true);
        when(tagDAO.isExists(anyLong())).thenReturn(true);
        when(certificateDAO.hasTags(anyLong(), anyLong())).thenReturn(true);

        boolean deleted = certificateService.deleteTag(certificate.getId(), tag.getId());
        assertTrue(deleted);
        verify(certificateDAO).deleteTag(1L, 1L);

        when(certificateDAO.isExists(anyLong())).thenReturn(true);
        when(tagDAO.isExists(anyLong())).thenReturn(true);
        when(certificateDAO.hasTags(anyLong(), anyLong())).thenReturn(true);

        assertThrows(CertificateAddTagException.class,
                () -> certificateService.addTag(certificate.getId(), tag.getId()));

    }

    @Test
    void shouldThrowCertificateNotFoundExceptionWhenCertificateDoesNotExist() {
        when(certificateDAO.isExists(anyLong())).thenReturn(false);

        assertThrows(CertificateNotFoundException.class,
                () -> certificateService.deleteTag(certificate.getId(), tag.getId()));
    }

    @Test
    void shouldThrowTagNotFoundExceptionWhenTagDoesNotExist() {
        when(certificateDAO.isExists(anyLong())).thenReturn(true);
        when(tagDAO.isExists(anyLong())).thenReturn(false);

        assertThrows(TagNotFoundException.class,
                () -> certificateService.deleteTag(certificate.getId(), tag.getId()));
    }

    @Test
    void shouldThrowCertificateAddTagExceptionWhenCertificateDoesNotHaveTag() {
        // Arrange
        when(certificateDAO.isExists(anyLong())).thenReturn(true);
        when(tagDAO.isExists(anyLong())).thenReturn(true);
        when(certificateDAO.hasTags(anyLong(), anyLong())).thenReturn(false);

        assertThrows(CertificateAddTagException.class,
                () -> certificateService.deleteTag(certificate.getId(), tag.getId()));
    }
}