package com.epam.esm.service.impl;

import com.epam.esm.dao.TagDAO;
import com.epam.esm.model.Tag;
import com.epam.esm.service.TagService;
import com.epam.esm.service.dto.TagDTO;
import com.epam.esm.service.dto.mapper.DtoMapper;
import com.epam.esm.service.dto.mapper.TagDtoMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TagServiceImplTest {

    @Mock
    private TagDAO tagDAO;
    private DtoMapper<Tag, TagDTO> tagDtoMapper;
    private TagService tagService;
    private final TagDTO tag = new TagDTO(1L, "Tag 1");

    @BeforeEach
    void setUp() {
        tagDtoMapper = new TagDtoMapper(new ModelMapper());
        tagService = new TagServiceImpl(tagDAO, tagDtoMapper);

    }

    @Test
    void getAllTest() {

        List<Tag> list = new ArrayList<>();
        list.add(new Tag(2L, "Tag 2"));
        list.add(new Tag(3L, "Tag 3"));
        list.add(new Tag(4L, "Tag 4"));

        when(tagDAO.getAll()).thenReturn(list);

        List<TagDTO> all = tagService.getAll();
        verify(tagDAO).getAll();

        assertEquals(3, all.size());

    }

    @Test
    void addTagTest() {

        tagService.add(tag);

        ArgumentCaptor<Tag> argumentCaptor =
                ArgumentCaptor.forClass(Tag.class);
        verify(tagDAO).add(argumentCaptor.capture());
        Tag value = argumentCaptor.getValue();
        assertEquals(value, tagDtoMapper.toEntity(tag));

        Tag entity = tagDtoMapper.toEntity(tag);
        when(tagDAO.add(any(Tag.class)))
                .thenReturn(entity);
        TagDTO added = tagService.add(tag);
        assertNotNull(added.getName());
    }

    @Test
    void getTest() {
        tagService.get(tag.getId());

        ArgumentCaptor<Long> argumentCaptor =
                ArgumentCaptor.forClass(Long.class);

        verify(tagDAO).get(argumentCaptor.capture());

        Long value = argumentCaptor.getValue();

        assertEquals(value, tagDtoMapper.toEntity(tag).getId());
    }

    @Test
    void deleteTest() {
        tagService.delete(tag.getId());
        verify(tagDAO).delete(tag.getId());

        when(tagDAO.delete(eq(tag.getId()))).thenReturn(true);

        assertTrue(tagService.delete(tag.getId()));
        assertFalse(tagService.delete(2L));
    }

    @Test
    void isExistsTest() {

        given(tagDAO.isExists(anyString())).willReturn(true);
        given(tagDAO.isExists(anyLong())).willReturn(false);

        boolean nameExists = tagService.isExists(tag.getName());
        boolean idExists = tagService.isExists(tag.getId());

        verify(tagDAO).isExists(anyString());
        assertTrue(nameExists);
        verify(tagDAO).isExists(anyLong());
        assertFalse(idExists);
    }

    @Test
    void updateTest() {
        assertThrows(UnsupportedOperationException.class, () ->
                tagService.update(tag));
    }

}